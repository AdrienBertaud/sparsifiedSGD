# -*- coding: utf-8 -*-
"""
Created on June 2021.

@author: Song Zhijie, Jiang Xiaowen, Adrien Bertaud

Gradient memory module

Keep unused gradient in memory and use them later.
Can be used with random or top k sparsifier.
"""
from struct import pack
from const import VERBOSE, float32_format, float32_number_of_bytes


def get_bytes_per_gradient_sgd(gradient_length):
    """Get number of bytes necessary to store quantized gradient."""
    return gradient_length * float32_number_of_bytes


def get_bytes_per_iterations_sgd(iters, grad_length):
    """Return a list of number of bytes necessary to compress gradient."""
    return iters * get_bytes_per_gradient_sgd(grad_length)


class SGD():
    """ModelParameters for rand k SGD."""

    def __init__(self):
        """Init ModelParameters."""
        self.number_of_bytes = None

    def get_name(self):
        """Get name to store the results."""
        name = "sgd"
        return name

    def quantize(self, gradient, sparse=False):
        """Get quantized gradient from full gradient."""
        if sparse:
            raise NotImplementedError

        if VERBOSE:
            print("\n*** quantize SGD ***")
        return gradient, len(gradient) * float32_number_of_bytes

    def get_number_of_bytes(self, gradient):
        """Get number of bytes necessary to store quantized gradient."""
        if not self.number_of_bytes:
            self.number_of_bytes = get_bytes_per_gradient_sgd(len(gradient))
        return self.number_of_bytes

    def compress_quantized_gradient(self, quantized_gradient):
        """Compress the gradient for communication."""
        vector_length = len(quantized_gradient)

        if VERBOSE:
            print("\n*** compress_quantized_gradient ***")
            print("quantized_gradient : ", type(quantized_gradient))
            print("vector_length : ", vector_length)

        compressed_quantized_gradient = bytearray()

        for i in range(vector_length):
            byte_array_float_32 = bytearray(pack(float32_format, quantized_gradient[i]))
            if VERBOSE:
                print(["0x%02x" % b for b in byte_array_float_32])
            compressed_quantized_gradient += byte_array_float_32

        if VERBOSE:
            print("compressed gradient = ", compressed_quantized_gradient,
                  type(compressed_quantized_gradient))

        return compressed_quantized_gradient
