# -*- coding: utf-8 -*-
"""
Created on June 2021.

@author: Song Zhijie, Jiang Xiaowen, Adrien Bertaud
"""
import numpy as np
from const import VERBOSE
from const import int32_format, max_accepted_vector_length, int32_number_of_bytes
from struct import pack


def get_bytes_per_gradient_sign_sgd(gradient_length):
    """
    Get average number of bytes necessary to store quantized gradient.

    We consider that in average 50% are positive.
    """
    return int(gradient_length/2) * int32_number_of_bytes


def get_bytes_per_iterations_sign_sgd(iters, grad_length):
    """Return a list of number of bytes necessary to compress gradient."""
    bytes_list = []
    bytes_per_gradient = get_bytes_per_gradient_sign_sgd(grad_length)

    for i in iters:
        bytes_list.append(i * bytes_per_gradient)

    return bytes_list


class Signed_SGD():
    """ModelParameters for signed SGD."""

    def __init__(self):
        """Init ModelParameters."""
        self.vector_length = 0

    def sparse_str(self):
        """Get sparse string."""
        sparse_str = "full"
        sparse_str += super().sparse_str()
        return sparse_str

    def get_name(self):
        """Get name to store the results."""
        name = "signedSGD"
        return name

    def quantize(self, gradient, sparse=False):
        """Get quantized gradient from full gradient."""
        if sparse:
            raise NotImplementedError

        self.vector_length = gradient.shape[0]
        signed_gradient = np.sign(gradient)
        return signed_gradient, self.get_number_of_bytes(signed_gradient)

    def get_number_of_bytes(self, signed_gradient):
        """Get number of bytes necessary to store quantized gradient.

        We only need to encode 1 or -1 indexes. We encodes 1 here.
        Returns the size of 1 indexes.
        """
        return np.sum(signed_gradient[signed_gradient > 0]) * int32_number_of_bytes

    def compress_quantized_gradient(self, quantized_gradient):
        """Compress the gradient for communication."""
        vector_length = len(quantized_gradient)

        if VERBOSE:
            print("\n*** compress_quantized_gradient ***")
            print("quantized_gradient : ", type(quantized_gradient))
            print("vector_length : ", vector_length)

        assert(vector_length <= max_accepted_vector_length)

        indexes = np.where(quantized_gradient > 0)[0]

        if VERBOSE:
            print("indexes : ", indexes)

        compressed_quantized_gradient = bytearray()

        for i in indexes:
            byte_array_unsigned_short_16 = bytearray(pack(int32_format, i))
            compressed_quantized_gradient += byte_array_unsigned_short_16

        if VERBOSE:
            print("compressed gradient = ", compressed_quantized_gradient,
                  type(compressed_quantized_gradient))

        return compressed_quantized_gradient
