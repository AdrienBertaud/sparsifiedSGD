# -*- coding: utf-8 -*-
"""
Created on June 2021.

@author: Song Zhijie, Jiang Xiaowen, Adrien Bertaud
"""
import glob
import os
import pickle
import matplotlib.pyplot as plt

from const import VERBOSE, error_msg_on_type_bytearray


def to_percent(x):
    """Convert to percent."""
    return round(100 * x, 2)


def pickle_it(var, name, directory):
    """Save results to pickle files in given directory."""
    with open(os.path.join(directory, "{}.pickle".format(name)), 'wb') as f:
        pickle.dump(var, f)


def load_results(experiment_dir):
    """Load results from rexperiment directory."""
    losses = load_pickles(os.path.join(experiment_dir, 'losses.pickle'))
    iters = load_pickles(os.path.join(experiment_dir, 'iters.pickle'))
    timers = load_pickles(os.path.join(experiment_dir, 'timers.pickle'))
    # bytes_total = load_pickles(os.path.join(experiment_dir, 'bytes_total.pickle'))

    return losses, iters, timers  # , bytes_total


def load_pickles(path):
    """Load pickle file."""
    with open(path, 'rb') as f:
        return pickle.load(f)


def unpickle_dir(d):
    data = {}
    assert os.path.exists(d), "{} does not exists".format(d)
    for file in glob.glob(os.path.join(d, '*.pickle')):
        name = os.path.basename(file)[:-len('.pickle')]
        with open(file, 'rb') as f:
            var = pickle.load(f)
        data[name] = var
    return data


def load_dataset(root_dir, pickle_file):
    """Load dataset from given pickle file."""
    dataset_file = os.path.join(get_data_dir(root_dir), pickle_file)

    print('load dataset : ', dataset_file)

    with open(dataset_file, 'rb') as f:
        X, y = pickle.load(f)

    return X, y


def create_dir(dir_name):
    """
    Create directory with given name if it doesn't exist.

    Return the directory name (no change on it, just for convenience).
    """
    if not os.path.exists(dir_name):
        print("Create directory : ", dir_name)
        os.mkdir(dir_name)
    return dir_name


def get_data_dir(root_dir):
    """Get data directory name."""
    dir = os.path.join(root_dir, "data")
    return create_dir(dir)


def get_result_dir(root_dir):
    """Get result directory name."""
    dir = os.path.join(root_dir, "results")
    return create_dir(dir)


def get_figures_dir(root_dir):
    """Get result directory name."""
    dir = os.path.join(root_dir, "figures")
    return create_dir(dir)


def get_experiment_directory(results_directory, quantization_method, model_parameters):
    """Get experiment directory where results are stored."""
    name = quantization_method.get_name() + "_" + model_parameters.get_name()
    return os.path.join(results_directory, name)


def get_number_of_bytes(compressed_quantized_gradient):
    """Return the number of bytes of the compressed quantized gradient."""
    if VERBOSE:
        print("\n*** get_number_of_bytes ***")
        print("compressed_quantized_gradient : ", compressed_quantized_gradient,
              type(compressed_quantized_gradient))

    if type(compressed_quantized_gradient) != bytearray:
        raise Exception(error_msg_on_type_bytearray)

    return len(compressed_quantized_gradient)


def plot_df(df, xlabel="", ylabel="", legend=""):
    """Plot dataframe."""
    df.plot(marker='o', alpha=0.7)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.legend(title=legend)


def plot(x, y, xlabel="", ylabel="", title=""):
    """Plot y versus x."""
    plt.plot(x, y)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
