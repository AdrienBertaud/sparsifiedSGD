# -*- coding: utf-8 -*-
"""
Created on June 2021.

@author: Song Zhijie, Jiang Xiaowen, Adrien Bertaud

General constants are defined there.
"""
VERBOSE = False  # Activate only for debug purpose
INIT_WEIGHT_STD = 0.01
LOSS_PER_EPOCH = 10
float32_format = "f"  # https://docs.python.org/3/library/struct.html
int32_format = "i"  # https://docs.python.org/3/library/struct.html
uint64_format = "Q"  # https://docs.python.org/3/library/struct.html
max_int32 = 2147483647
max_float32 = 3.4028235e38
float32_number_of_bytes = 4
int32_number_of_bytes = 4
max_accepted_vector_length = 47236
error_msg_on_type_bytearray = "Compressed gradient must be of type bytearray!"
