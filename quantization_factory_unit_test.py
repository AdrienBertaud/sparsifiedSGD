# -*- coding: utf-8 -*-
"""
Created on June 2021.

@author: Song Zhijie, Jiang Xiaowen, Adrien Bertaud
"""
import unittest
from quantization_factory import get_quantization_method
from qsgd import Quantized_SGD
# from signed_sgd import Signed_SGD
from take_k import Take_K
from sgd import SGD

number_of_components = 9


class TestQuantizationFactory(unittest.TestCase):
    """Test quantization factory."""

    def test_get_quantization_method_sgd(self):
        """Test get_quantization_method with SGD."""
        quantization_method = get_quantization_method(
            model_name='sgd',
            k=number_of_components)

        self.assertEqual(type(quantization_method), SGD)

    def test_get_quantization_method_qsgd(self):
        """Test get_quantization_method with QSGD."""
        quantization_method = get_quantization_method(
            model_name='qsgd',
            k=number_of_components)

        self.assertEqual(type(quantization_method), Quantized_SGD)
        self.assertEqual(quantization_method.qsgd_s, number_of_components)

    def test_get_quantization_method_rand(self):
        """Test get_quantization_method with QSGD."""
        quantization_method = get_quantization_method(
            model_name='rand',
            k=number_of_components)

        self.assertEqual(type(quantization_method), Take_K)
        self.assertEqual(quantization_method.k, number_of_components)
        self.assertEqual(quantization_method.get_name(),
                         "rand_{}".format(number_of_components))


if __name__ == '__main__':
    unittest.main()
