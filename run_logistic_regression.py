# -*- coding: utf-8 -*-
"""
Created on June 2021.

@author: Song Zhijie, Jiang Xiaowen, Adrien Bertaud
"""
import os
import pathlib

from logistic_parallel import LogisticParallelSGD
from parameters import Parameters
from utils import pickle_it, get_result_dir, create_dir, get_experiment_directory, load_dataset
from quantization_factory import get_quantization_method


def parallel_experiment(root_dir, X, y, model_parameters, quantization_method,
                        loss_per_epoch=10):
    """Make parallel experiments."""
    print('start experiment')

    directory = get_experiment_directory(get_result_dir(root_dir),
                                         quantization_method,
                                         model_parameters)
    print('Will save results in "{}"'.format(directory))

    if os.path.exists(directory):
        if len(os.listdir(directory)) > 0:
            print("Experiment has already been run, stopping.")
            return
    else:
        create_dir(directory)

    m = LogisticParallelSGD(model_parameters)
    iters, timers, losses, bytes_total = m.fit(X, y,
                                               num_features=X.shape[1],
                                               num_samples=X.shape[0],
                                               loss_per_epoch=loss_per_epoch)

    pickle_it(iters, 'iters', directory)
    pickle_it(timers, 'timers', directory)
    pickle_it(losses, 'losses', directory)
    pickle_it(bytes_total, 'bytes_total', directory)

    print('results saved in "{}"'.format(directory))


if __name__ == '__main__':
    """For debug only. Refer to notebook for real experiments."""
    root_dir = pathlib.Path().absolute()
    print("root_dir :", root_dir)

    X, y = load_dataset(root_dir, "rcv1.pickle")

    n, d = X.shape
    max_cores = 1
    model_name = "qsgd"
    repeat = 1
    k = 10

    model_parameters = Parameters(
        num_epoch=1,
        n_cores=max_cores,
        lr_type='bottou',
        initial_lr=10.,
        regularizer=1 / n,
        estimate='mean')

    quantization_method = get_quantization_method(model_name, k=k)

    parallel_experiment(
        root_dir=root_dir,
        X=X,
        y=y,
        model_parameters=model_parameters,
        quantization_method=quantization_method,
        loss_per_epoch=100)
