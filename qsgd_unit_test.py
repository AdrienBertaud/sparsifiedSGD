# -*- coding: utf-8 -*-
"""
Created on June 2021.

@author: Song Zhijie, Jiang Xiaowen, Adrien Bertaud
"""
import unittest
import numpy as np
from qsgd import Quantized_SGD
from const import error_msg_on_type_bytearray, max_float32, max_accepted_vector_length,\
    float32_number_of_bytes, int32_number_of_bytes
from utils import get_number_of_bytes


class TestQSGD(unittest.TestCase):
    """Test QSGD."""

    def test_quantize_level_1(self):
        """Test quantize with level 1."""
        qsgd_s = 1
        quantized_sgd = Quantized_SGD(qsgd_s)

        x = np.array([1, 1])
        quantized_gradient, number_of_bytes = quantized_sgd.quantize(x)
        print("quantized_gradient = ", quantized_gradient)
        print("number_of_bytes = ", number_of_bytes)
        expected_result_1 = np.array([1.41421356, 0.])
        expected_result_2 = np.array([0., 0.])
        expected_result_3 = np.array([1.41421356, 1.41421356])
        expected_result_4 = np.array([0., 1.41421356])
        self.assertTrue(
            (quantized_gradient.all() == expected_result_1.all()) |
            (quantized_gradient.all() == expected_result_2.all()) |
            (quantized_gradient.all() == expected_result_3.all()) |
            (quantized_gradient.all() == expected_result_4.all()))
        self.assertEqual(number_of_bytes,
                         (float32_number_of_bytes + int32_number_of_bytes) * qsgd_s
                         + np.count_nonzero(quantized_gradient) * int32_number_of_bytes)

        x = np.array([1, -1])
        quantized_gradient, number_of_bytes = quantized_sgd.quantize(x)
        print("quantized_gradient = ", quantized_gradient)
        print("number_of_bytes = ", number_of_bytes)
        expected_result_1 = np.array([1.41421356, 0.])
        expected_result_2 = np.array([0., 0.])
        expected_result_3 = np.array([1.41421356, -1.41421356])
        expected_result_4 = np.array([0., -1.41421356])
        self.assertTrue(
            (quantized_gradient.all() == expected_result_1.all()) |
            (quantized_gradient.all() == expected_result_2.all()) |
            (quantized_gradient.all() == expected_result_3.all()) |
            (quantized_gradient.all() == expected_result_4.all()))
        self.assertEqual(number_of_bytes,
                         (float32_number_of_bytes + int32_number_of_bytes) * qsgd_s
                         + np.count_nonzero(quantized_gradient) * int32_number_of_bytes)

    def test_quantize_level_2(self):
        """Test quantize with level 1."""
        qsgd_s = 2
        quantized_sgd = Quantized_SGD(qsgd_s)

        # x = np.full(100, 1)
        x = np.array([3, 1])

        quantized_gradient, number_of_bytes = quantized_sgd.quantize(x)
        print("qsgd_s = ", quantized_sgd.qsgd_s)
        print("quantized_gradient = ", quantized_gradient)
        print("number_of_bytes = ", number_of_bytes)
        expected_result_1 = np.array([3.16227766, 1.58113883])
        expected_result_2 = np.array([3.16227766, 0.])
        expected_result_3 = np.array([1.58113883, 1.58113883])
        self.assertTrue(
            (quantized_gradient.all() == expected_result_1.all())
            | (quantized_gradient.all() == expected_result_2.all())
            | (quantized_gradient.all() == expected_result_3.all())
            # | (quantized_gradient.all() == expected_result_4.all())
        )
        self.assertEqual(number_of_bytes,
                         (float32_number_of_bytes + int32_number_of_bytes) * qsgd_s
                         + np.count_nonzero(quantized_gradient) * int32_number_of_bytes)

    # def test_compress_uncompress_quantized_gradient(self):
    #     """Test tp compress and uncompress quantized gradient."""
    #     quantized_gradient = np.array([0., 0., 0., -2])
    #     quantized_sgd.vector_length = len(quantized_gradient)
    #     compressed_gradient = quantized_sgd.compress_quantized_gradient(quantized_gradient)
    #     # print("compressed_gradient = ", compressed_gradient)
    #     # expected_result = bytearray(b'\x00\x00\x00\xc0\xfc\xff\xff\xff')
    #     # self.assertTrue((compressed_gradient == expected_result))

    #     uncompressed_gradient = quantized_sgd.uncompress(compressed_gradient)
    #     print("uncompressed_gradient = ", uncompressed_gradient)
    #     for i in range(len(quantized_gradient)):
    #         self.assertAlmostEqual(quantized_gradient[i], uncompressed_gradient[i])

    # def test_compress_quantized_gradient_input_too_long(self):
    #     """Test compress_quantized_gradient with too big input."""
    #     quantized_gradient = np.ones(max_accepted_vector_length+1)
    #     try:
    #         quantized_sgd.compress_quantized_gradient(quantized_gradient)
    #     except Exception as e:
    #         print(e)
    #     else:
    #         print("An exception should have been raised.")
    #         self.assertTrue(False)

    # def test_compress_quantized_gradient_close_to_limits(self):
    #     """Test compress_quantized_gradient close to max length and values limits."""
    #     quantized_gradient = np.zeros(max_accepted_vector_length)
    #     quantized_gradient[max_accepted_vector_length-1] = max_float32
    #     compressed_gradient = quantized_sgd.compress_quantized_gradient(quantized_gradient)
    #     print("compressed_gradient = ", compressed_gradient)
    #     expected_result = bytearray(b'\xff\xff\x7f\x7f\x84\xb8\x00\x00')
    #     self.assertTrue((compressed_gradient == expected_result))

    # def test_get_number_of_bytes(self):
    #     """Test get_number_of_bytes."""
    #     compressed_quantized_gradient = bytearray(b'\xf3\x04\xb5?\x00\x00')
    #     number_of_bytes = get_number_of_bytes(compressed_quantized_gradient)
    #     self.assertEqual(number_of_bytes, 6)

    # def test_get_number_of_bytes_invalid_type(self):
    #     """Test get_number_of_bytes with invalid type."""
    #     compressed_quantized_gradient = [1, 1]
    #     try:
    #         get_number_of_bytes(compressed_quantized_gradient)
    #     except Exception as e:
    #         print(e)
    #         self.assertTrue(e.args[0] == error_msg_on_type_bytearray)
    #     else:
    #         print("An exception should have been raised.")
    #         self.assertTrue(False)


if __name__ == '__main__':
    unittest.main()
