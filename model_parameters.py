class ModelParameters:
    """
    ModelParameters class used for all the experiments, redefine a string representation to summarize the experiment
    """

    def __init__(self,
                 # model,
                 # model_parameter,
                 num_epochs,
                 lr_type,
                 initial_lr=None,
                 l2_by_n=10,
                 epoch_decay_lr=None,
                 take_k=None,
                 take_top=False,
                 with_memory=False,
                 estimate='final',
                 name=None,
                 num_cores=1,
                 tau=None,
                 real_update_every=1,
                 # qsgd_s=None,
                 # sign_sgd=False
                 ):
        """
        Init ModelParameters.

        # model: choices=['top', 'rand', 'sgd', 'qsgd', 'sign_sgd']
        # model_argument: parameter associated to the model
        """
        # if model == top
        # {
        #     take_top = True
        #     take_k = model_parameter
        # }

        # a lot of sanity checks to fail fast if we have inconsistent ModelParameters
        assert num_epochs >= 0
        assert lr_type in ['constant', 'epoch-decay', 'decay', 'bottou']

        if lr_type in ['constant', 'decay']:
            assert initial_lr > 0
        if lr_type == 'decay':
            assert initial_lr and tau
            assert l2_by_n > 0
        if lr_type == 'epoch-decay':
            assert epoch_decay_lr is not None

        # if take_k and model == 'sgd':
        #     take_k = None
        #     print('k ignored')

        if not take_k:
            assert not take_top and not with_memory

        assert estimate in ['final', 'mean', 't+tau', '(t+tau)^2']

        # if qsgd_s is not None:
        #     assert take_k is None and real_update_every == 1

        # if sign_sgd == True:
        #     assert take_k is None and qsgd_s is None and not with_memory

        assert num_cores >= 1

        self.num_epochs = num_epochs
        self.lr_type = lr_type
        self.initial_lr = initial_lr
        self.l2_by_n = l2_by_n
        self.epoch_decay_lr = epoch_decay_lr
        self.take_k = take_k
        self.take_top = take_top
        self.with_memory = with_memory
        self.estimate = estimate
        self.name = name
        self.num_cores = num_cores
        self.tau = tau
        self.real_update_every = real_update_every
        # self.qsgd_s = qsgd_s

    def __str__(self):
        if self.name:
            return self.name

        lr_str = self.lr_str()
        sparse_str = self.sparse_str()

        reg_str = ""
        if self.l2_by_n:
            reg_str = "-reg{}".format(self.l2_by_n)

        return "epoch{}-{}{}-{}-{}".format(self.num_epochs, lr_str, reg_str, sparse_str, self.estimate)

    def get_name(self):
        """Get name to store the results."""
        name = "epo{}_lrt{}_lr{}".format(
            self.num_epochs,
            self.lr_type,
            self.initial_lr,
            # self.model,
            # epoch_decay_lr,
            # take_k,
            # take_top,
            # with_memory,
            # estimate,
            # num_cores,
            # tau,
            # real_update_every,
            # self.qsgd_s
        )
        return name

    def lr_str(self):
        lr_str = ""
        if self.lr_type == 'constant':
            lr_str = "lr{}".format(self.initial_lr)
        elif self.lr_type == 'decay':
            lr_str = "lr{}decay{}".format(self.initial_lr, self.epoch_decay_lr)
        elif self.lr_type == 'custom':
            lr_str = "lr{}/lambda*(t+{})".format(self.initial_lr, self.tau)
        elif self.lr_type == 'bottou':
            lr_str = "lr-bottou-{}".format(self.initial_lr)
        else:
            lr_str = "lr-{}".format(self.lr_type)

        return lr_str

    def sparse_str(self):
        if not self.take_k:
            sparse_str = "full"
        else:
            if self.take_top:
                sparse_str = "top{}".format(self.take_k)
            else:
                sparse_str = "rand{}".format(self.take_k)

            if self.with_memory:
                sparse_str += "-with-mem"
            else:
                sparse_str += "-no-mem"
        return sparse_str

    def __repr__(self):
        return "Parameter('{}')".format(str(self))

    # def quantize(self, gradient, sparse=False):
    #     """Get quantized gradient from full gradient."""
    #     return None
