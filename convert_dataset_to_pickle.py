# -*- coding: utf-8 -*-
"""
Created on June 2021.

@author: Song Zhijie, Jiang Xiaowen, Adrien Bertaud
"""
import pickle
import os
from sklearn.datasets import load_svmlight_file
from const import get_data_dir

path_bz2 = os.path.join(get_data_dir(""), 'rcv1_test.binary.bz2')

X, y = load_svmlight_file(path_bz2)

path_pickle = os.path.join(get_data_dir(""), 'rcv1_test.binary.bz2')

with open(path_pickle, 'wb') as f:
    pickle.dump((X, y), f)
