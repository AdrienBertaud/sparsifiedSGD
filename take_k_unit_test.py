# -*- coding: utf-8 -*-
"""
Created on June 2021.

@author: Song Zhijie, Jiang Xiaowen, Adrien Bertaud
"""
import unittest
import numpy as np
from take_k import Take_K
from const import error_msg_on_type_bytearray, max_float32, max_accepted_vector_length, float32_number_of_bytes, int32_number_of_bytes
from math import isclose

quantized_sgd = Take_K(top_or_rand='rand', k=1)


class TestTakeK(unittest.TestCase):
    """Test take k values quantization."""

    def test_quantize(self):
        """Test quantize."""
        x = np.array([1, 1])
        quantized_gradient, number_of_bytes = quantized_sgd.quantize(x)
        print("quantized_gradient = ", quantized_gradient)
        expected_result_1 = np.array([1, 0.])
        expected_result_2 = np.array([0., 1])
        self.assertTrue(
            (quantized_gradient.all() == expected_result_1.all()) |
            (quantized_gradient.all() == expected_result_2.all()))
        # self.assertEqual(number_of_bytes, float32_number_of_bytes + int32_number_of_bytes)

    def test_compress_uncompress_quantized_gradient_not_sparse(self):
        """Test compress uncompress quantized gradient."""
        quantized_gradient = np.array([1, 0.])
        compressed_gradient = quantized_sgd.compress_quantized_gradient_not_sparse(
            quantized_gradient)
        print("compressed_gradient = ", compressed_gradient)

        quantized_gradient = quantized_sgd.uncompress(compressed_gradient)
        print("quantized_gradient = ", quantized_gradient)
        for i in range(len(quantized_gradient)):
            self.assertTrue(isclose(quantized_gradient[i], quantized_gradient[i]))

    def test_compress_quantized_gradient_not_sparse_input_too_long(self):
        """Test compress_quantized_gradient_not_sparse with too big input."""
        quantized_gradient = np.ones(max_accepted_vector_length+1)
        try:
            quantized_sgd.compress_quantized_gradient_not_sparse(quantized_gradient)
        except Exception as e:
            print(e)
        else:
            print("An exception should have been raised.")
            self.assertTrue(False)

    def test_compress_quantized_gradient_not_sparse_close_to_limits(self):
        """Test compress_quantized_gradient_not_sparse close to max length and values limits."""
        quantized_gradient = np.zeros(max_accepted_vector_length)
        quantized_gradient[max_accepted_vector_length-1] = max_float32
        quantized_sgd.vector_length = len(quantized_gradient)
        compressed_gradient = quantized_sgd.compress_quantized_gradient_not_sparse(
            quantized_gradient)
        print("compressed_gradient = ", compressed_gradient)
        # expected_result = bytearray(b'\xff\xff\x7f\x7f\x83\xb8\x00\x00')
        # self.assertTrue((compressed_gradient == expected_result))

        uncompressed_gradient = quantized_sgd.uncompress(compressed_gradient)
        print("uncompressed_gradient = ", uncompressed_gradient)
        for i in range(len(quantized_gradient)):
            self.assertTrue(isclose(quantized_gradient[i], quantized_gradient[i]))

    # def test_uncompress(self):
    #     """Test uncompress function."""
    #     compressed_quantized_gradient = bytearray(b'\x00\x00\x80?\x00\x00')
    #     quantized_gradient = quantized_sgd.uncompress(compressed_quantized_gradient)
    #     print("quantized_gradient = ", quantized_gradient)
    #     expected_quantized_gradient = np.zeros(self.vector_length)
    #     expected_quantized_gradient[0] = 1
    #     for i in range(len(expected_quantized_gradient)):
    #         self.assertAlmostEqual(quantized_gradient[i], expected_quantized_gradient[i])


if __name__ == '__main__':
    unittest.main()
