# -*- coding: utf-8 -*-
"""
Created on Sun Apr  4 08:59:21 2021.

@author: Song Zhijie, Jiang Xiaowen, Adrien Bertaud Adrien Bertaud.

Run all unit tests in current directory.
"""
import unittest


if __name__ == '__main__':
    print("\nLaunch all unit tests!")
    testsuite = unittest.TestLoader().discover('.', pattern='*unit_test.py')
    unittest.TextTestRunner(verbosity=1).run(testsuite)
    print("\nEnd of unit testing.")
