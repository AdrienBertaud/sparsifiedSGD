# -*- coding: utf-8 -*-
"""
Created on June 2021.

@author: Song Zhijie, Jiang Xiaowen, Adrien Bertaud

Gradient memory module

Keep unused gradient in memory and use them later.
Can be used with random or top k sparsifier.
"""
import numpy as np
from const import VERBOSE
from struct import pack, unpack
from const import int32_format, float32_format, int32_number_of_bytes,\
    float32_number_of_bytes, max_accepted_vector_length


def get_bytes_per_gradient_take_k(k):
    """Get number of bytes necessary to store quantized gradient.

    Returns size for float followed by its index.
    """
    return (float32_number_of_bytes + int32_number_of_bytes) * k


def get_bytes_per_iterations_take_k(iters, k):
    """Return a list of number of bytes necessary to compress gradient."""
    return iters * get_bytes_per_gradient_take_k(k)


class Take_K():
    """ModelParameters for top k SGD."""

    def __init__(self, top_or_rand, k):
        """Init ModelParameters."""
        assert((top_or_rand == 'top') or (top_or_rand == 'rand'))

        self.k = k
        self.top_or_rand = top_or_rand
        self.vector_length = 0
        self.number_of_bytes = None

    def get_name(self):
        """Get name to store the results."""
        name = "{}_{}".format(self.top_or_rand, self.k)
        return name

    def create_memory(self, gradient, with_memory=False, memory=None):
        # def create_memory(self, gradient):
        """Create the memory if does not exist."""
        if with_memory:
            if memory is None:
                memory = np.zeros(gradient.shape, dtype=np.float64)
            memory += gradient
        else:
            memory = gradient
        return memory

    def quantize(self, gradient, sparse=False):
        """Get quantized gradient from full gradient."""
        self.create_memory(gradient)
        self.vector_length = gradient.shape[0]
        memory = gradient

        d = np.prod(memory.shape)
        k = min(self.k, d)

        if self.top_or_rand == 'top':
            indices = np.argpartition(np.abs(memory.ravel()), -k)[-k:]
        elif self.top_or_rand == 'rand':
            indices = np.random.choice(d, k, replace=False)

        if VERBOSE:
            print("memory = ", memory, type(memory), memory.shape)
            print("d = ", d, type(d))
            print("k = ", k, type(k))
            print("indices = ", indices, type(indices))

        if not sparse:
            out_grad = np.zeros_like(memory)
            out_grad[indices] = memory[indices]
        else:
            out_grad = (indices, memory[indices])

        return out_grad, self.get_number_of_bytes()

    def get_number_of_bytes(self):
        """Get number of bytes necessary to store quantized gradient."""
        if not self.number_of_bytes:
            get_bytes_per_gradient_take_k(self.k)
        return self.number_of_bytes

    def compress_quantized_gradient(self, quantized_gradient, sparse=False):
        """Compress the gradient for communication."""
        if sparse:
            self.compress_quantized_gradient_sparse(
                quantized_gradient[0], quantized_gradient[1])
        else:
            self.compress_quantized_gradient_not_sparse(quantized_gradient)

    def compress_quantized_gradient_not_sparse(self, quantized_gradient):
        """Compress the gradient for communication."""
        vector_length = len(quantized_gradient)

        if VERBOSE:
            print("\n*** compress_quantized_gradient ***")
            print("quantized_gradient : ", type(quantized_gradient))
            print("vector_length : ", vector_length)

        # The compression codes indexes as unsigned integers on 16 bits.
        assert(vector_length <= max_accepted_vector_length)

        indexes = np.where(quantized_gradient != 0)[0]

        if VERBOSE:
            print("indexes : ", indexes)

        compressed_quantized_gradient = bytearray()

        for i in indexes:
            # if VERBOSE:
            #     print("i : ", i)
            #     print("quantized_gradient[i] : ", quantized_gradient[i],
            #           type(quantized_gradient[i]))
            byte_array_float_32 = bytearray(pack(float32_format, quantized_gradient[i]))
            # if VERBOSE:
            #     print(["0x%02x" % b for b in byte_array_float_32])
            byte_array_uint = bytearray(pack(int32_format, i))
            compressed_quantized_gradient += byte_array_float_32 + byte_array_uint

        if VERBOSE:
            print("compressed gradient = ", compressed_quantized_gradient,
                  type(compressed_quantized_gradient))
            # print(["0x%02x" % b for b in compressed_quantized_gradient])

        return compressed_quantized_gradient

    def compress_quantized_gradient_sparse(self, non_nul_values, indexes):
        """Compress the gradient for communication."""
        if VERBOSE:
            print("\n*** compress_quantized_gradient ***")
            print("non_nul_values : ", type(non_nul_values))
            print("indexes : ", indexes)

        assert(np.max(indexes) <= max_accepted_vector_length)

        if VERBOSE:
            print("indexes : ", indexes)

        compressed_quantized_gradient = bytearray()

        for i in range(len(indexes)):
            if VERBOSE:
                print("index : ", indexes[i])
                print("component value : ", non_nul_values[i], type(non_nul_values[i]))

            byte_array_float_32 = bytearray(pack(float32_format, non_nul_values[i]))

            if VERBOSE:
                print(["0x%02x" % b for b in byte_array_float_32])

            byte_array_uint = bytearray(pack(int32_format, indexes[i]))
            compressed_quantized_gradient += byte_array_float_32 + byte_array_uint

        if VERBOSE:
            print("compressed gradient = ", compressed_quantized_gradient,
                  type(compressed_quantized_gradient))

        return compressed_quantized_gradient

    def uncompress(self, compressed_quantized_gradient):
        """Uncompress the compressed quantized gradient."""
        assert(self.vector_length > 0)

        if VERBOSE:
            print("\n*** uncompress ***")
            print("compressed_quantized_gradient : ", compressed_quantized_gradient,
                  type(compressed_quantized_gradient))

        indexes = []

        for i in range(0, len(compressed_quantized_gradient),
                       float32_number_of_bytes+int32_number_of_bytes):
            j = i + float32_number_of_bytes
            float_bytes = compressed_quantized_gradient[i:j]
            value = unpack(float32_format, float_bytes)[0]
            if VERBOSE:
                print("value = ", value, type(value))
            int_bytes = compressed_quantized_gradient[j:j+int32_number_of_bytes]
            index = unpack(int32_format, int_bytes)[0]
            indexes.append(index)

        if VERBOSE:
            print("indexes = ", indexes, type(indexes))

        uncompressed_quantized_gradient = np.zeros(self.vector_length)
        uncompressed_quantized_gradient[indexes] = value

        return uncompressed_quantized_gradient
