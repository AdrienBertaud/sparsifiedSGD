# -*- coding: utf-8 -*-
"""
Created on June 2021.

@author: Song Zhijie, Jiang Xiaowen, Adrien Bertaud

Quantization scheme for QSGD.
"""
import numpy as np
from struct import pack, unpack
from const import VERBOSE
from const import int32_format, float32_format, int32_number_of_bytes,\
    float32_number_of_bytes, max_accepted_vector_length


class Quantized_SGD():
    """ModelParameters for QSGD."""

    def __init__(self, qsgd_s):
        """Init ModelParameters."""
        assert(qsgd_s > 0)

        self.qsgd_s = qsgd_s
        self.vector_length = 0

    def get_name(self):
        """Get uniquely named suffix for the results."""
        name = "qsgd_s_{}".format(self.qsgd_s)
        return name

    def quantize(self, gradient, sparse=False):
        """
        Quantize the gradient in s level on the absolute value coef wise.
        
        Follows Alistarh, 2017 (https://arxiv.org/pdf/1610.02132.pdf).
        """
        if sparse:
            raise NotImplementedError

        if VERBOSE:
            print("\n*** quantize ***")
            print("gradient : ", gradient, gradient.shape, type(gradient))
            print("self.qsgd_s : ", self.qsgd_s, type(self.qsgd_s))

        self.vector_length = gradient.shape[0]

        norm = np.sqrt(np.sum(np.square(gradient)))
        if VERBOSE:
            print("norm = ", norm)

        level_float = self.qsgd_s * np.abs(gradient) / norm
        if VERBOSE:
            print("level_float = ", level_float)

        previous_level = np.floor(level_float)
        if VERBOSE:
            print("previous_level = ", previous_level)

        is_next_level = np.random.rand(*gradient.shape) < (level_float - previous_level)
        if VERBOSE:
            print("is_next_level = ", is_next_level)

        new_level = previous_level + is_next_level
        if VERBOSE:
            print("new_level = ", new_level)

        result = np.sign(gradient) * norm * new_level / self.qsgd_s
        if VERBOSE:
            print("result = ", result, type(result))

        return result, self.get_number_of_bytes(new_level)

    def get_number_of_bytes(self, new_level):
        """Get number of bytes necessary to store quantized gradient."""
        value_and_its_nb_of_occurences = float32_number_of_bytes + int32_number_of_bytes

        return self.qsgd_s * value_and_its_nb_of_occurences +\
            np.count_nonzero(new_level) * int32_number_of_bytes

    def compress_quantized_gradient(self, quantized_gradient):
        """Compress the gradient for communication."""
        if VERBOSE:
            print("\n*** compress_quantized_gradient ***")
            print("quantized_gradient : ", type(quantized_gradient))
            print("len(quantized_gradient) : ", len(quantized_gradient))

        # The compression codes indexes as unsigned integers on 16 bits.
        assert(len(quantized_gradient) <= max_accepted_vector_length)

        max_value = np.max(quantized_gradient)

        if max_value > 0:
            norm = max_value
        else:
            norm = -np.min(quantized_gradient)

        if VERBOSE:
            print("norm : ", norm, type(norm))

        byte_array_float_32 = bytearray(pack(float32_format, norm))

        if VERBOSE:
            print(["0x%02x" % b for b in byte_array_float_32])

        compressed_quantized_gradient = byte_array_float_32

        indexes = np.where(quantized_gradient != 0)[0]

        if VERBOSE:
            print("indexes : ", indexes)

        for i in (indexes + 1):
            byte_array_unsigned_short_16 = bytearray(
                pack(int32_format, int(i * np.sign(quantized_gradient[i-1]))))
            compressed_quantized_gradient += byte_array_unsigned_short_16

        if VERBOSE:
            print("compressed gradient = ", compressed_quantized_gradient,
                  type(compressed_quantized_gradient))
            # print(["0x%02x" % b for b in compressed_quantized_gradient])

        return compressed_quantized_gradient

    def uncompress(self, compressed_quantized_gradient):
        """Uncompress the compressed quantized gradient."""
        assert(self.vector_length > 0)

        if VERBOSE:
            print("\n*** uncompress ***")
            print("compressed_quantized_gradient : ", compressed_quantized_gradient,
                  type(compressed_quantized_gradient))

        float_bytes = compressed_quantized_gradient[0:float32_number_of_bytes]
        value = unpack(float32_format, float_bytes)[0]

        signed_indexes = []
        for i in range(4, len(compressed_quantized_gradient), int32_number_of_bytes):
            int_bytes = compressed_quantized_gradient[i:i+int32_number_of_bytes]
            signed_index = unpack(int32_format, int_bytes)[0]
            signed_indexes.append(signed_index)

        if VERBOSE:
            print("value = ", value, type(value))
            print("signed_indexes = ", signed_indexes, type(signed_indexes))

        uncompressed_quantized_gradient = np.zeros(self.vector_length)

        for signed_index in signed_indexes:
            index = np.abs(signed_index) - 1
            uncompressed_quantized_gradient[index] = value * np.sign(signed_index)

        return uncompressed_quantized_gradient
