# -*- coding: utf-8 -*-
"""
Based on https://github.com/epfml/sparsifiedSGD.

@inproceedings{scj2018sparseSGD,
  author = {Sebastian U. Stich and Jean-Baptiste Cordonnier and Martin Jaggi},
  title = "{Sparsified {SGD} with Memory}",
  booktitle = {NIPS 2018 - Advances in Neural Information Processing Systems},
  year = 2018,
  url = {https://arxiv.org/abs/1809.07599}

Modified on June 2021
Modifiers: Song Zhijie, Jiang Xiaowen, Adrien Bertaud
"""
import numpy as np
import sgd
import qsgd
import signed_sgd


class GradientMemory:
    def __init__(self, take_k=None, take_top=False, with_memory=False, qsgd_s=None, sign_sgd=False):
        self.with_memory = with_memory
        self.take_top = take_top
        self.take_k = take_k
        self.qsgd_s = qsgd_s
        self.m = None
        self.sign_sgd = sign_sgd

    def __call__(self, g, sparse=False):

        number_of_bytes = 0

        if self.sign_sgd:
            sign = signed_sgd.Signed_SGD()
            out_grad, _ = sign.quantize(g)
            return out_grad, number_of_bytes

        if self.qsgd_s:
            out_grad, number_of_bytes = qsgd.Quantized_SGD(self.qsgd_s).quantize(g)
            return out_grad, number_of_bytes

        if not self.take_k:
            out_grad, _ = sgd.SGD().quantize(g)
            return out_grad, number_of_bytes

        if self.with_memory:
            # create the memory if does not exist
            if self.m is None:
                self.m = np.zeros(g.shape, dtype=np.float64)
            self.m += g
        else:
            self.m = g

        # for k < 1 sometimes no gradient is used from the memory
        # if no_apply:
        #     return None

        d = np.prod(self.m.shape)
        k = min(self.take_k, d)
        if self.take_top:
            indices = np.argpartition(np.abs(self.m.ravel()), -k)[-k:]
        else:
            indices = np.random.choice(d, k, replace=False)

        if not sparse:
            out_grad = np.zeros_like(self.m)
            out_grad[indices] = self.m[indices]
        else:
            out_grad = (indices, self.m[indices])
        self.m[indices] = 0.

        return out_grad, number_of_bytes
