# -*- coding: utf-8 -*-
"""
Created on Tue Jun 15 21:52:51 2021.

@author: Song Zhijie, Jiang Xiaowen, Adrien Bertaud
"""
from qsgd import Quantized_SGD
from signed_sgd import Signed_SGD
from take_k import Take_K
from sgd import SGD


def get_quantization_method(model_name, k):
    """
    Get quantization method.

    Parameters
    ----------
    model_name: choices=['top', 'rand', 'sgd', 'qsgd', 'sign_sgd']
    k:  number of value to take from gradient or number of quantization levels for qsgd
    """
    if model_name == 'sgd':
        return SGD()
    elif model_name == 'qsgd':
        return Quantized_SGD(qsgd_s=k)
    elif model_name == 'sign_sgd':
        return Signed_SGD()
    elif (model_name == 'top') or (model_name == 'rand'):
        return Take_K(model_name, k=k)

    return None
