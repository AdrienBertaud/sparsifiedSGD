# -*- coding: utf-8 -*-
"""
Created on June 2021.

@author: Song Zhijie, Jiang Xiaowen, Adrien Bertaud
"""
import unittest
from utils import get_number_of_bytes
from const import error_msg_on_type_bytearray


class TestTakeK(unittest.TestCase):
    """Test utils."""

    def test_get_number_of_bytes(self):
        """Test get_number_of_bytes."""
        compressed_quantized_gradient = bytearray(b'\x00\x00\x80?\x00\x00\x00\x00')
        number_of_bytes = get_number_of_bytes(compressed_quantized_gradient)
        self.assertEqual(number_of_bytes, len(compressed_quantized_gradient))

    def test_get_number_of_bytes_invalid_type(self):
        """Test get_number_of_bytes with invalid type."""
        compressed_quantized_gradient = [1, 1]
        try:
            get_number_of_bytes(compressed_quantized_gradient)
        except Exception as e:
            print(e)
            self.assertTrue(e.args[0] == error_msg_on_type_bytearray)
        else:
            print("An exception should have been raised.")
            self.assertTrue(False)

    # def test_uncompress(self):
    #     """Test uncompress function."""
    #     compressed_quantized_gradient = bytearray(b'\x00\x00\x80?\x00\x00')
    #     quantized_gradient = quantized_sgd.uncompress(compressed_quantized_gradient)
    #     print("quantized_gradient = ", quantized_gradient)
    #     expected_quantized_gradient = np.zeros(self.vector_length)
    #     expected_quantized_gradient[0] = 1
    #     for i in range(len(expected_quantized_gradient)):
    #         self.assertAlmostEqual(quantized_gradient[i], expected_quantized_gradient[i])


if __name__ == '__main__':
    unittest.main()
