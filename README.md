# Communication efficiency with Quantized SGD
### CS-439 Optimization for machine learning - EPFL
Song Zhijie, Jiang Xiaowen, Adrien Bertaud

In this project, we explore several quantization and sparcification schemes as Quantized SGD, top-k, rand-k and SignSGD. Experimentation has be run on a one layer logistic regression classifier and a convolutional network classifier. We compared learning speed and precision of the methods. Our results give also an insight of the gain for gradient communication efficiency.


### Get RCV1 dataset for logistic regression
```
Dowload to data directory:
https://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/binary/rcv1_test.binary.bz2

Must be converted to pickle file. To do so, run :
convert_dataset_to_pickle.py
```

### Logistic regression experiments
```
run_logistic_regression_experiments.ipynb
run_logistic_regression_post_processing.ipynb
```

### Image classification experiments
```
MNIST dataset is automatically downloaded by the Notebook:
run_MNIST_classification_experiments.ipynb
```

### Run all unit tests
```
unit_tests.py
```

### Specific libraries versions
```
torch: 1.8.1+cu101
```


